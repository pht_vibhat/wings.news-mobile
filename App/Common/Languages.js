import LocalizedStrings from 'react-native-localization'

/* API
 setLanguage(languageCode) - to force manually a particular language
 getLanguage() - to get the current displayed language
 getInterfaceLanguage() - to get the current device interface language
 formatString() - to format the passed string replacing its placeholders with the other arguments strings
 */


const Languages = new LocalizedStrings({
	ar: {
		//Root (Home)
		home: 'باش بەت',
		readlater: 'كىيىن كۆرەي',
		category: 'ھەسىپىلەر',
		back: ' قايتىش',

		//Login Form
		passwordUp: 'شىفىر',
		passwordnor: 'شىفىر',
		forgotPassword: 'شىفىر ئىسىمدە يوق',
		login: 'مەرھابا',
		noAccount: 'تىزىملىتىش',
		signup: 'مەرھەمەت',
		// MenuSide
		news: 'ئەسەرلەر',
		contact: 'ئالاقىلىشىش',
		aboutus: 'تورىسىدا',
		setting: 'تەڭشەك',
		logout: "چىقىپ كىتىش",

		// Post
		post: 'يازما',
		posts: 'يازما',
		feature: 'ئالقىشلانغان ئەسەر',
		days: 'كۈن',
		editorchoice: 'تاللانغان',

		// PostDetail
		comment: 'ئىنكاس',
		yourcomment: 'ئىنكاسىڭىز',
		relatedPost: 'مۇناسىۋەتلىك يازمىلار',

		all: 'بەت',
		forLife: 'قىشلانغان ئەسەر',
		powerBy: 'ئەسەر',
		video: 'ئەسەر',
		fontSize: 'قىشلان',
		email: 'قىشلان',
		enterEmail: 'قىشلان قىشلان',
		enterPassword: 'قىشلان قىشلان',
		photo: 'قىشلان',
		clear: 'قىشلان',
		by: 'قىشلان',
		name: 'قىشلان',
		enterName: 'قىشلان',
		send: 'قىشلان',
		commentSubmit: 'قىشلان',
		recent: 'قىشلان'
	},
	en: {
		//Root (Home)
		home: 'Home',
		readlater: 'Read Later',
		category: 'Category',
		back: ' Back',

		//Login Form
		passwordUp: 'PASSWORD',
		passwordnor: 'password',
		forgotPassword: 'Forget password',
		for: 'Forgot Password',
		login: 'Log In',
		noAccount: 'Do not have an account?',
		signup: 'Get started',

		// MenuSide
		news: 'News',
		contact: 'Contact',
		aboutus: 'About Us',
		setting: 'Setting',
		logout: "Logout",

		// Post
		post: 'Post',
		posts: 'Posts',
		feature: 'Feature articles',
		days: 'days',
		editorchoice: 'Editor Choice',

		// PostDetail
		comment: 'Comment',
		yourcomment: 'Your Comment',
		relatedPost: 'Related Post',

		all: 'All',
		forLife: 'for lifestyle people',
		powerBy: 'Power by Wings',
		video: 'Video',
		fontSize: 'Content font size',
		email: 'EMAIL',
		enterEmail: 'Enter your email',
		enterPassword: 'Type your password',
		photo: 'Photo',
		clear: 'Clear',
		by: "by",
		name: 'NAME',
		enterName: 'Enter name',
		send: 'Send',
		commentSubmit: 'Your Comment is sent and waiting for approving',
		recent: 'Recent Posts'
	},

	vi: {
		//Root (Home)
		home: 'Trang chủ',
		readlater: 'Đọc sau',
		category: 'Chuyên mục',
		back: 'Quay lại',

		//Login Form
		passwordUp: 'MẬT KHẨU',
		passwordnor: 'Mật khẩu',
		forgotPassword: 'Quên mật khẩu',
		login: 'Đăng nhập',
		noAccount: 'Bạn chưa có tài khoản?',
		signup: 'Đăng ký',

		// MenuSide
		news: 'Tin tức',
		contact: 'Liên hệ',
		aboutus: 'Về chúng tôi',
		setting: 'Thiết lập',
		logout: "Thoát",

		// Post
		post: 'Bài viết',
		posts: 'Bài viết',
		feature: 'Bài viết nổi bật',
		days: 'ngày',
		editorchoice: 'Editor Choice',

		// PostDetail
		comment: 'Bình luận',
		yourcomment: 'Bình luận của bạn',
		realtedPost: 'Bài viết liên quan',

		all: 'Tất cả',
		forLife: 'cho người sành điệu',
		powerBy: 'Tạo bởi Wings',
		video: 'Phim',
		fontSize: 'Font size của nội dung',
		email: 'Địa chỉ email',
		enterEmail: 'Gõ địa chỉ email',
		enterPassword: 'Gõ mật khẩu của bạn',
		photo: "Hình ảnh",
		clear: 'Xóa',
		by: "Bởi",
		name: 'Tên',
		enterName: 'Gõ Tên của bạn',
		send: 'Send',
		commentSubmit: 'Comment của bạn đang được review',
		recent: 'Bài Viết Gần đây'
	},
	///Put other languages here
})

export default Languages
