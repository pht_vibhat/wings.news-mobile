import WordpressApi from "./WordpressApi"
import Constants from '@common/Constants';

var Api = new WordpressApi({
    url: Constants.URL.root,
    logo: Constants.URL.logo
});

export default Api;
