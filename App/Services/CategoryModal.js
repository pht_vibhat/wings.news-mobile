import React, {Component} from "react";
import {AsyncStorage} from "react-native";
import Constants from "@common/Constants";
import _ from "lodash"

const selectedCategory = [];

function CategoryModal() {
	if (!(this instanceof CategoryModal)) {
		return new CategoryModal();
	}
};



CategoryModal.prototype.postSettingsCategories = function (data) {
	this.saveSettingsData(data);
};

CategoryModal.prototype.saveSettingsData = async function (data) {
	//selectedCategory = data;
	try {
		await AsyncStorage.setItem(Constants.Key.categories, JSON.stringify(data));
	} catch (error) {
		console.log(error);
	}
};

CategoryModal.prototype.clearSettingsData = function () {

	return AsyncStorage.multiRemove([Constants.Key.categories], (err) => {});
}
CategoryModal.prototype.getSettingsCategories = async function () {
	//return selectedCategory;
	try {
		const userData = await AsyncStorage.getItem(Constants.Key.categories);

		if (userData != null) {
			return JSON.parse(userData);
		}
	} catch (error) {
		// console.log(error);
	}
};


export default CategoryModal;

