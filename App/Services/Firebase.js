import * as firebase from 'firebase';
import Constants from "@common/Constants";

const firebaseApp = firebase.initializeApp(Constants.Firebase);
firebaseApp.getCurrentUser = function (){
    return firebaseApp.auth().currentUser;
};

export default firebaseApp;
