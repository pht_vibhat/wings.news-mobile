'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import {Actions} from "react-native-router-flux";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import css from "@common/style";
import AppEventEmitter from "@services/AppEventEmitter";
import IconImage from "./IconImage";
import Constants from '@common/Constants';
import Languages from "@common/Languages";

export default class Toolbar extends Component {
	constructor(props) {
		super(props);
		this.state = {layout: Constants.Post.simple_view}
	}
	open() {
		AppEventEmitter.emit('hamburger.click');
	}

	changeLayout(layout) {
		console.log('changeLayout');

		this.setState({layout: layout});
		AppEventEmitter.emit('news.changeLayout', layout);
		AppEventEmitter.emit('readlater.changeLayout');
	}

	render() {
		const self = this;
		const logo = () => {
				if (typeof Constants.logo != 'undefined')	{
						return <Image source={Constants.wingsLogo} style={css.toolbarLogo}></Image>;
					}
					return <Text style={[css.toolbarTitle, self.props.textColor]}>{Constants.AppName}</Text>
				};

		const homeButton = function () {
			if (typeof self.props.isChild != 'undefined') {
				return (
					<View style={{flexDirection: 'row', alignItems: 'center'}}>
						<TouchableOpacity onPress={self.props.action ? self.props.action : Actions.pop}>
							<Icon name={'arrow-left'} size={16} color="#333"
							      style={[css.iconBack, self.props.textColor]}/>
							<Text style={[css.textBack, {marginLeft: 20}]}>{Languages.back}</Text>
						</TouchableOpacity>

						<TouchableOpacity onPress={Actions.home} style={css.toolbarHome}>{logo()}</TouchableOpacity>
					</View>
				);
			}
			return (
				<View>
					<View style={{flexDirection: 'row', zIndex: 99, left: -6, top: -10, alignItems: 'center'}}>
						<IconImage action={self.open} image={{uri: Constants.icons.home}}/>
					</View>
					<TouchableOpacity onPress={self.open} style={css.toolbarTitleView}>
						{typeof self.props.name != 'undefined' ?
							<Text style={[css.toolbarTitle, self.props.textColor]}>{self.props.name}</Text> :
							logo()
						}
					</TouchableOpacity>
				</View>
			);
		};

		return (
			<View style={[css.toolbarMenu, this.props.css]}>
				{homeButton()}

				<View style={{flexDirection: 'row', alignItems: 'center'}}>
					{self.props.cardButton &&
					<IconImage cssImage={[{marginRight: 4}, this.state.layout != 1 && css.iconHide]}
					           action={this.changeLayout.bind(this, 1)}
					           image={{uri: Constants.icons.card}}/> }

					{self.props.newsLayoutButton &&
					<IconImage  cssImage={[{marginRight: 4}, this.state.layout != 3 && css.iconHide] }
					            action={this.changeLayout.bind(this, 3)}
					            image={{uri: Constants.icons.layout}}/>}

          {self.props.listButton &&
					<IconImage cssImage={[{marginRight: 4}, this.state.layout != 2 && css.iconHide]}
										 action={this.changeLayout.bind(this, 2)}
										 image={{uri: Constants.icons.cardView}}/> }

          {self.props.listViewButton &&
					<IconImage cssImage={[{marginRight: 0}, this.state.layout != 4 && css.iconHide]}
										 action={this.changeLayout.bind(this, 4)}
										 image={{uri: Constants.icons.listView}}/> }

					{self.props.searchButton &&
					<IconImage image={{uri: Constants.icons.search}}/> }
				</View>
			</View>
		);
	}
}
