'use strict';
import React, {Component} from "react";
import {View, TouchableHighlight, Text} from 'react-native';
import css from './style';
import Constants from "@common/Constants"
import {AdMobBanner, AdMobInterstitial} from 'react-native-admob';

export default class Index extends Component {
  componentWillUnmount() {
    AdMobInterstitial.removeAllListeners();
  }

  showInterstital() {
    AdMobInterstitial.showAd((error) => error && console.log(error));
  }

  interstitialDidClose() {
    AdMobInterstitial.requestAd((error) => error && console.log(error));
  }

  render() {
    return (
      <View style={css.body}>
        <AdMobBanner
          ref={component => this._root = component}
          bannerSize={'fullBanner'}
          testDeviceIDs={[Constants.AdMob.deviceID]}
          adUnitID={Constants.AdMob.unitID}
        />
      </View>
    )
  }
}
