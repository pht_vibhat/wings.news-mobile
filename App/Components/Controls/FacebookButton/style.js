import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Color from '@common/Color';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "alignItems": "center",
        "backgroundColor": "#435FAC",
        "borderRadius": 40,
        "paddingTop": 5,
        "paddingRight": 5,
        "paddingBottom": 5,
        "paddingLeft": 5,
        "marginBottom": 14
    }
});