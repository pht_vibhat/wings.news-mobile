import React, {Component} from "react";
import {WebView, View, Text, Image, Dimensions, Linking, TouchableOpacity, AsyncStorage} from "react-native";
import HTML from "react-native-render-html";
import Tools from "@common/Tools";
import Constants  from "@common/Constants";
const {width, height, scale} = Dimensions.get("window");

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSize: Constants.fontText.size
    };

    Tools.getFontSizePostDetail().then((data) => {
      this.setState({fontSize: data})
    })
  }

	render() {
		const htmlContent = this.props.html;
		// let htmlContent = striptags(this.props.html, ['img', 'a', 'p', 'span', 'br', 'ul', 'ol', 'li', 'h1', 'h2', 'h3', 'h4']);
		const styles = {
			a: {zIndex: 9,
				fontSize: this.state.fontSize != null ? 15 : 15,
				color: '#333',
				fontWeight: '500',
				textDecorationLine: 'underline'},
			p: {color: '#333333'},
			h1: { backgroundColor: '#FF0000' },
			img: { resizeMode: 'cover' }
		}

		const renderers = {
			table: (htmlAttribs, children, passProps) =>
			{
				return (
					<View style={{flex: 1, borderWidth: Number(htmlAttribs.border), padding:Number(htmlAttribs.cellpadding)}} {...passProps} >
						{children}
					</View>
				)
			},
			tbody: (htmlAttribs, children, passProps) =>
			{
				return (
					<View {...passProps} >
						{children}
					</View>
				)
			},
			tr: (htmlAttribs, children, passProps) =>
			{
				return (
					<View style={{flex: 1, marginLeft: 10, marginRight: 10 }} {...passProps} >
						{children}
					</View>
				)
			} ,
			td: (htmlAttribs, children, passProps) => {
				return (
					<View {...passProps} style={{flex: 1, borderWidth: Number(htmlAttribs.border)}} >
						{children}
					</View>
				)
			},
			// a: (htmlAttribs, children, passProps) => {
			//  return (
			//   <Text {...passProps} style={{
			// 	  zIndex: 9,
			// 	  fontSize: this.state.fontSize != null ? 15 : 15,
			// 	  color: '#333',
			// 	  fontWeight: '500',
			// 	  textDecorationLine: 'underline'
			//   }}
			//   >
			// 	  {children}
			//   </Text>
			//  )
			// },
			h1: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps} style={{
						fontSize: this.state.fontSize != null ? 15 : 15,
						color: '#333'
					}}>
						{children}
					</Text>
				)
			},
			h2: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps} style={{
						fontSize: this.state.fontSize != null ? 15 : 15,
						color: '#333'
					}}>
						{children}
					</Text>
				)
			},
			h4: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps} style={{
						fontSize: this.state.fontSize != null ? 15 : 15,
						color: '#333'
					}}>
						{children}
					</Text>
				)
			},
			p: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps} style={{
						fontSize: this.state.fontSize != null ? 15 : 15,
						color: '#333'
					}}>
						{children}
					</Text>
				)
			},

			div: (htmlAttribs, children, passProps) =>
			{
				return (
					<View {...passProps} style={{flexDirection: 'column'}}>
						{children}
					</View>
				)
			},
			img: (htmlAttribs, children, passProps) => {
				return (
					<Image source={{uri: htmlAttribs.src}} />
				)
			},
			br: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps}>
						{children}
					</Text>
				)
			},
			b: (htmlAttribs, children, passProps) => {
				return (
					<Text {...passProps} style={{fontSize: 15, color: '#333'}}>
						{children}
					</Text>
				)
			}
		}

		return <View style={{padding: 12}}>
			<HTML
				html={htmlContent}
				renderers={renderers}
				onLinkPress={(evt, href) => Tools.openLink(href)}
				htmlStyles={styles}
			/>
		</View>
	}
}
