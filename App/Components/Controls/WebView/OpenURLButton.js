import React, {Component} from "react";
import {View, Text, Linking, TouchableOpacity} from "react-native";
import HTML from "react-native-fence-html";


export default class Index extends Component {
	handleClick()	{
    Linking.canOpenURL(this.props.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.url);
      } else {
        // console.log('Don\'t know how to open URI: ' + this.props.url);
      }
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleClick.bind(this)}>
          <View style={this.props.css} {...this.props} />
      </TouchableOpacity>
    );
  }
}
