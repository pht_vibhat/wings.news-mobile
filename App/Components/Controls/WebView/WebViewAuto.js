import React, {Component} from "react";
import WebView from './WebViewAutoHeight';
import Constants  from "@common/Constants";
import Tools from "@common/Tools";

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSize: Constants.fontText.size
    };

    Tools.getFontSizePostDetail().then((data) => {
      this.setState({fontSize: data})
    })
  }

  render() {
    const customStyle = "<style>* {max-width: 100%;} body {padding: 4px; font-size: " + this.state.fontSize + "px; font-family: " + Constants.fontFamily + ";}</style>";
    const htmlContent = this.props.html;

		return (
			<WebView html={customStyle + htmlContent} />
		)
	}
}
