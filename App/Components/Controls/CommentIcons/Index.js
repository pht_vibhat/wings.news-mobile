'use strict';
import React, {Component} from "react";
import {View, Text, Share, Linking} from 'react-native';
import css from './style';
import Icon from "react-native-vector-icons/SimpleLineIcons";
import User from "@services/User";
import Tools from "@common/Tools";

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ColorHeart: typeof this.props.color == 'undefined' ? '#333' : this.props.color
    };
  }

  readLater() {
    this.setState({
      ColorHeart: '#e74c3c'
    })
    User.savePost(this.props.post);
  }

  shareText() {
    const post = this.props.post ? this.props.post : "";
    const url = post.link;
    const title = typeof post.title.rendered != 'undefined' ? Tools.getDescription(post.title.rendered, 300) : "";
		let sharedMessage = `${title}, ${post.link} - Over 100,00 global news agencies- Download Android App https://goo.gl/QLsoXW | Download ios App https://goo.gl/KxxE6t`
    Share.share({
        message: sharedMessage,
        url: url,
        title: title
      },
      {
        dialogTitle: 'Share:  ' + title,
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ],
        tintColor: 'blue'
      }).catch((error) => this.setState({result: 'error: ' + error.message}));
  }

  externalLink() {
    var url = this.props.post.link ? this.props.post.link : "";
    Tools.openLink(url);
  }

  render() {
    const textColor = typeof this.props.color == 'undefined' ? '#333' : this.props.color;
    return (
      <View
        style={typeof (this.props.style) == 'undefined' || this.props.style == null ? css.shareIcon : this.props.style}>

        {typeof this.props.hideLoveIcon == 'undefined' ?
          <Icon.Button style={css.newsIcons}
                       name="heart"
                       size={16}
                       color={this.state.ColorHeart}
                       onPress={this.readLater.bind(this)}
                       backgroundColor="transparent">
          </Icon.Button>
          : null }

        {typeof this.props.hideShareIcon == 'undefined' ?
          <Icon.Button style={css.newsIcons}
                       onPress={this.shareText.bind(this)}
                       name="share"
                       size={16}
                       color={textColor}
                       backgroundColor="transparent">
          </Icon.Button>
          : null }

        {typeof this.props.hideOpenIcon == 'undefined' ?
          <Icon.Button style={css.newsIcons}
                       name="share-alt"
                       size={16}
                       color={textColor}
                       onPress={this.externalLink.bind(this)}
                       backgroundColor="transparent">
          </Icon.Button>
          : null }

        {typeof this.props.hideCommentIcon == 'undefined' ?
          <Icon.Button style={css.newsIcons}
                       name="speech"
                       size={16}
                       color={textColor}
                       backgroundColor="transparent">
            <Text style={[css.iconText, {color: textColor}]}>{this.props.comment}</Text>
          </Icon.Button>
          : null }

      </View>
    )
  }
}
