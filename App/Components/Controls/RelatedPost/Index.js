'use strict';
import React, {Component} from "react";
import {Text, View, Image, ListView, TouchableOpacity} from "react-native";
import {Actions} from "react-native-router-flux";
import TimeAgo from "react-native-timeago";
import Api from "@services/Api";
import css from "@post/style";
import CommentIcons from "@controls/CommentIcons/Index";
import Tools from "@common/Tools";
import Languages from "@common/Languages";
import Spinkit from "@controls/Spinkit/Index";

export default class RelatedPost extends Component {
	constructor(props) {
		super(props);
		this.data = [];
		this.category = this.props.post ? this.props.post.categories[0] : '';
		this.postCurrent = this.props.post ? this.props.post.id : '';
		this.state = {
			page: 1,
			limit: 6,
			isLoading: true,
			hasData: false,
			finish: false,
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		}
	}

	componentWillMount() {
		this.fetchPostData();
	}

	fetchPostData(isReload) {
		var self = this;
		let postData = {
			'per_page': this.state.limit,
			'page': this.state.page,
			'categories': this.category,
			'exclude': this.postCurrent,
			'sticky': false
		};

		if (typeof isReload !== 'undefined') {
			postData.page = 1;

			// this important to refresh the list view data source to change layout row
			self.data = [];
		}

		self.setState({isLoading: true});

		Api.getPosts(postData)
			.then(function (data) {
				// console.log('posts data', data);

				if (typeof data != 'undefined') {
					self.data = self.data.concat(data);

					// if (self.refs.listview)	{
					self.setState({
						page: self.state.page + 1,
						hasData: true,
						finish: data.length < self.state.limit,
						isLoading: false,
						dataSource: self.getDataSource(self.data)
					});
					// }
				}
				else {
					self.setState({finish: true});
				}
			});
	}

	getDataSource(posts) {
		return this.state.dataSource.cloneWithRows(posts);
	}

	/**
	 * Render row for the list view
	 * @param post
	 * @param sectionID
	 * @param rowID
	 * @returns {*}
	 */
	renderRow(post, sectionID, rowID) {
		if (typeof (post.title) == 'undefined') {
			return null;
		}

		const imageUrl = Tools.getImage(post);

		const authorName = post._embedded.author[0]['name'];

		const commentCount = typeof post._embedded.replies == 'undefined' ? 0 : post._embedded.replies[0].length;

		const postTitle = typeof post.title == 'undefined' ? '' : post.title.rendered;

		// list view layout
		return (
			<TouchableOpacity style={css.smCardNews} onPress={Actions.postDetails.bind(this, {post: post})}>
				<View style={css.cardView}>
					<Image style={css.smImage} source={{uri: imageUrl}}></Image>
					<View style={css.smDescription}>
						<Text style={css.smTitle}>{Tools.getDescription.bind(this)(postTitle) }</Text>
						<Text style={css.smAuthor}><TimeAgo time={post.date} hideAgo={true}/> by @{authorName}</Text>
					</View>

					<CommentIcons hideOpenIcon={true} post={post} style={css.smShareIcons} comment={commentCount}/>
				</View>
			</TouchableOpacity>
		);
	}

	render() {
		return (
			<View style={css.body}>
				<Text style={css.relatedPostText}>{Languages.relatedPost}</Text>

				{this.state.hasData && <ListView
					ref="listview"
					contentContainerStyle={css.listView}
					enableEmptySections={true}
					onEndReachedThreshold={200}
					onEndReached={this.fetchPostData.bind(this)}
					dataSource={this.state.dataSource}
					renderRow={this.renderRow.bind(this)}>
				</ListView> }

				<Spinkit isLoading={this.state.isLoading}/>
			</View>
		);
	}
}
