'use strict';

import React, {Component} from "react";
import {
	View,
	ListView,
	TouchableOpacity,
	TextInput,
	Image,
	Text
} from "react-native";

import TimeAgo from "react-native-timeago";
import HTML from "react-native-fence-html";
import css from './style'
import Languages from "@common/Languages";
import Api from "@services/Api";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import User from "@services/User";

export default class Index extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
			txtComment: '',
			addComment: false,
      userData: ''
		};
    this.fetchDataUser();
	}

  fetchDataUser() {
    var self = this;
    User.getUser().then((data) => {
      self.setState({
        userData: data
      });
    });
  }


	componentWillMount() {
		this.fetchComment();
	}

	fetchComment() {
		if (this.props.comments != null) {
			this.setState({dataSource: this.getDataSource(this.props.comments)});
		}

	}

	getDataSource(comments) {
		return this.state.dataSource.cloneWithRows(comments);
	}

	renderRow(comment) {
		const htmlStyle = {
			p: {color: '#888', fontSize: 14, lineHeight: 22}
		}
		const contentCmt = comment.content.rendered;
		const authorName = comment.author_name;
		const timePostCmt = comment.date;
		return (
			<View style={css.itemComment}>
				<View style={css.itemHeadComment}>
					<Image source={require('@images/icon-set/profile.png')} style={css.avatarComment}/>
					<View>
						<Text style={css.authorName}>{authorName}</Text>
						<Text style={css.timeAgoText}><TimeAgo time={timePostCmt} hideAgo={false}/></Text>
					</View>
				</View>
				<View style={css.commentHTML}>
					<HTML htmlStyles={htmlStyle} html={contentCmt}/>
				</View>
			</View>
		)
	}


	submitComment() {
		var self = this;
		var post = self.props.post;
		const author = post.author ? post.author : "1";
		const id = post.id;
		console.log('User : ',this.state.userData)
		const user = self.state.userData
		var commentData = {
			'post': id,
			'author_name': user.displayName,
			'content': this.state.txtComment,
			'author_email': user.email
		};

		// console.log('comment data', commentData);

		Api.createComment(commentData)
			.then(function (data) {
				if (data != null) {
					self.setState({
						addComment: true,
						txtComment: ''
					});
				}
			});
	}

	render() {
		if (!this.state.userData)	{
			return null;
		}

		return (
			<View style={css.wrapComment}>
				<Text style={css.headCommentText}>{Languages.comment}</Text>


				{this.state.addComment ? <Text>{Languages.commentSubmit}</Text> :
					<View>
						<View style={css.inputCommentWrap}>
							<TextInput style={css.inputCommentText}
							           underlineColorAndroid="transparent"
							           autoCorrect={false}
							           multiline={true}
							           onChangeText={text => this.setState({txtComment: text})}
							           placeholder={Languages.yourcomment}
							           onSubmitEditing={this.submitComment.bind(this)}
							/>
						</View>
						<TouchableOpacity onPress={this.submitComment.bind(this)} style={css.sendView}>
							<Icon name="cursor" size={16}
							      color="white"
							      style={css.sendButton}>
							</Icon>
							<Text style={css.sendText}>{Languages.send}</Text>
						</TouchableOpacity>
					</View>
				}

				<View style={css.wrapListComment}>
					<ListView enableEmptySections={true}
					          dataSource={this.state.dataSource}
					          renderRow={this.renderRow.bind(this)}>
					</ListView>
				</View>
			</View>
		)
	}
}
