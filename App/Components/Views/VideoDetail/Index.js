'use strict';

import React, {Component} from "react";
import {Text, View, Image, Dimensions, ScrollView, Platform} from "react-native";
import TimeAgo from "react-native-timeago";
import css from "./style";
import Toolbar from "@controls/Toolbar";
import Comment from "@controls/Comment/Index";
import WebView from "@controls/WebView/Index";
import RelatedPost from "@relatedpost/Index";
import Tools from "@common/Tools";
import CommentIcons from "@controls/CommentIcons/Index";
import Video from "@controls/Video/Index";


export default class Index extends Component {
	render() {
		const video = this.props.video;
		const authorName = video._embedded.author[0]['name'];
		const videoUrl = video.content.rendered != null ? Tools.getLinkVideo(video.content.rendered) : '';
		const videoTitle = video.title.rendered != null ? video.title.rendered : 'No Title';
		const commentCount = typeof video._embedded.replies == 'undefined' ? 0 : video._embedded.replies[0].length;

		return (
			<View style={css.body}>
				<Toolbar isChild={true} />

				<ScrollView style={Platform.OS === 'android' ? css.videoBodyAndroid:css.videoBody }>
					<Video source={videoUrl} style={css.video}/>

					<View style={css.description}>
						<Text style={css.author}><TimeAgo time={video.date} hideAgo={true}/> by @{authorName}</Text>
						<CommentIcons post={this.props.video} comment={commentCount}/>
					</View>

					<Text style={css.titleVideoText}>{Tools.getDescription(videoTitle, 300)}</Text>

					<WebView html={video.content.rendered} />

					<Comment
						comments={typeof (video._embedded.replies) == 'undefined' ? null : video._embedded.replies[0]}/>

					<RelatedPost post={this.props.video}/>

				</ScrollView>
			</View>
		);
	}
}
