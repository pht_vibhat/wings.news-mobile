import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Color from '@common/Color';
import Constants from '@common/Constants';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "width": width,
        "backgroundColor": Color.main
    },
    "video": {
        "height": vh * 40
    },
    "videoBody": {
        "paddingTop": 50,
        "backgroundColor": "white"
    },
    "videoBodyAndroid": {
        "paddingTop": 10,
        "backgroundColor": "white"
    },
    "titleVideoText": {
        "marginTop": 18,
        "marginLeft": 16,
        "marginRight": 14,
        "width": width - 30,
        "color": Color.title,
        "fontSize": 17,
        "lineHeight": 22,
        "fontWeight": "500"
    },
    "timeText": {
        "fontSize": 14,
        "marginLeft": 16,
        "marginTop": 8,
        "color": Color.time
    },
    "wrapLikeShare": {
        "marginTop": 5,
        "marginRight": 10,
        "marginBottom": 20,
        "flexDirection": "row",
        "justifyContent": "flex-end"
    },
    "author": {
        "color": "#999",
        "fontSize": 13,
        "fontWeight": "600",
        "marginTop": 12,
        "marginRight": 12,
        "marginBottom": 12,
        "marginLeft": 12
    },
    "description": {
        "backgroundColor": "#eee",
        "flexDirection": "row"
    }
});