import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Color from '@common/Color';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "flex": 1,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "backgroundColor": "#4EC1AC",
        "justifyContent": "center",
        "alignItems": "center"
    },
    "tab": {
        "paddingBottom": 0,
        "borderBottomWidth": 0,
        "paddingTop": 0,
        "paddingLeft": 0,
        "paddingRight": 0
    },
    "activeTab": {
        "height": 0,
        "backgroundColor": Color.main
    },
    "backgroundVideo": {
        "position": "absolute",
        "top": 0,
        "left": 0,
        "bottom": 0,
        "right": 0
    },
    "toolbar": {
        "left": 14,
        "alignSelf": "flex-start"
    }
});