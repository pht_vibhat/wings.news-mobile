'use strict';

import React, {Component} from "react";
import {View, Platform, Dimensions} from "react-native";
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view";
import css from "./style"
import SignIn from "@signin/Index";
import SignUp from "@signup/Index";
import Languages from "@common/Languages";
import DropdownAlert from 'react-native-dropdownalert';
import AppEventEmitter from "@services/AppEventEmitter";
import Constants from '@common/Constants';
import IconImage from "@controls/IconImage";

export default class Login extends Component {

  componentWillMount() {
    var self = this;

    AppEventEmitter.addListener('login.showError', this.showError.bind(this));
    AppEventEmitter.addListener('login.showInfo', this.showInfo.bind(this));
  }


  showError(message) {
    this.dropdown.alertWithType('error', 'Error', message)
  }

  open() {
    AppEventEmitter.emit('hamburger.click');
  }

  showInfo(message) {
    this.dropdown.alertWithType('info', 'Info', message)
  }

  render() {
    return (
      <View style={css.body}>
        <View style={css.toolbar}>
          <IconImage action={this.open} image={{uri: Constants.icons.homeWhite}} />
        </View>

        <ScrollableTabView
          initialPage={0}
          locked={false}
          tabBarUnderlineStyle={css.activeTab}
          tabBarActiveTextColor={"#fff"}
          tabBarInactiveTextColor={"rgba(255, 255, 255, 0.5)"}
          tabBarTextStyle={{fontWeight: '200', fontSize: 25}}
          contentProps={{margin: 0}}
          renderTabBar={() => <ScrollableTabBar
            underlineHeight={0}
            style={{borderBottomColor: 'transparent'}}
            tabsContainerStyle={{paddingLeft: 0, paddingRight: 0}}
            tabStyle={css.tab}
          />}>

          {Constants.RTL ? <SignUp tabLabel={Languages.signup} /> : <SignIn tabLabel={Languages.login} />}

          {Constants.RTL ? <SignIn tabLabel={Languages.login} /> : <SignUp tabLabel={Languages.signup} /> }

        </ScrollableTabView>

        <DropdownAlert ref={(ref) => this.dropdown = ref} />
      </View>
    );
  }
}
