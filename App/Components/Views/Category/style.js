import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
import Color from '@common/Color';
import Constants from '@common/Constants';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "backgroundColor": Color.main,
        "height": height
    },
    "listView": {
        "paddingTop": 50,
        "justifyContent": "flex-start",
        "flexDirection": "row",
        "flexWrap": "wrap"
    },
    "listViewAndroid": {
        "paddingTop": 10,
        "justifyContent": "flex-start",
        "flexDirection": "row",
        "flexWrap": "wrap"
    },
    "loading": {
        "marginTop": vh * 30
    },
    "spinnerView": {
        "height": height,
        "alignItems": "center",
        "justifyContent": "center"
    },
    "boxCategory": {
        "height": width/2 - 40,
        "width": width/2 - 12,
        "marginBottom": 8,
        "marginLeft": 8,
        "borderRadius": 2,
        "elevation": 5,
        "shadowColor": "#000",
        "shadowOpacity": 0.4,
        "shadowRadius": 2,
        "shadowOffset": {width: 0, height: 2}
    },
    "imageBox": {
        "borderRadius": 2,
        "overflow": "hidden",
        "alignItems": "center",
        "justifyContent": "center",
        "height": width/2 - 40,
        "width": width/2 - 12,
        "position": "relative"
    },
    "boxName": {
        "flex": 1,
        "justifyContent": "center",
        "alignItems": "center",
        "position": "relative",
        "zIndex": 9999
    },
    "boxNameText": {
        "color": "white",
        "fontSize": 18,
        "fontWeight": "400",
        "backgroundColor": "transparent"
    },
    "boxCountText": {
        "color": "white",
        "fontWeight": "400",
        "fontSize": 12,
        "marginTop": 6,
        "backgroundColor": "transparent"
    },
    "overlayVideo": {
        "top": 0,
        "left": 0,
        "zIndex": 100,
        "width": width,
        "height": (width/2),
        "position": "absolute",
        "backgroundColor": "rgba(0, 0, 0, .5)"
    }
});