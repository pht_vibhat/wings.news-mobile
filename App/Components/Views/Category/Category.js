'use strict';

import React, {Component} from "react";
import {Text, Platform, View, TouchableOpacity, Image} from "react-native";
import css from "./style";
import Constants from "@common/Constants";
import Languages from "@common/Languages";
import Tools from "@common/Tools";

export default class Category extends Component {
	constructor(props) {
		super(props);
	}

	viewCateDetail(id)	{
		Tools.viewCateDetail(id);
	}

	render() {
		const data = this.props.category;
		const imageURL =  typeof Constants.imageCategories[data.slug] != 'undefined' ? Constants.imageCategories[data.slug] : require("@images/category/though.png");
		const nameCate = data.name != "" ? data.name : '';
		const countPostByCate = data.count != "" ? data.count : '';

		return (
			<TouchableOpacity style={css.boxCategory} onPress={this.viewCateDetail.bind(this, data.id) }>
				<Image source={imageURL} style={css.imageBox}>
					<View style={css.boxName}>
						<Text style={css.boxNameText}>{nameCate.replace('&amp;', '&')}</Text>
						<Text style={css.boxCountText}>{countPostByCate} {Languages.posts}</Text>
					</View>
					<View style={css.overlayVideo}></View>
				</Image>
			</TouchableOpacity>
		);
	}
}
