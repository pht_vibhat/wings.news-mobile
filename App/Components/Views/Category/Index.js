'use strict';

import React, {Component} from "react";
import {
	ListView,
  Platform,
	WebView,
	Text,
	TextInput,
	View,
	Image,
	TouchableOpacity,
	ScrollView,
	StyleSheet,
	Dimensions,
	AppRegistry
} from "react-native";
import {Actions} from "react-native-router-flux";
import css from "@category/style";
import Toolbar from "@controls/Toolbar";
import Api from "@services/Api";
import CategoryControl from "./Category";
import Spinkit from "@controls/Spinkit/Index";
import Constants from "@common/Constants";
import Languages from "@common/Languages";

export default class Category extends Component {
	constructor(props) {
		super(props);
		this.data = [];
		this.state = {
			isLoading: false,
			finish: false,
			hasData: false,
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		}
	}

	componentDidMount() {
		this.fetchCategoriesData();
	}

	fetchCategoriesData(isReload) {
		var self = this;
		if (this.state.finish || this.state.isLoading) {
			return;
		}
		let postData = {
			'per_page': 100,
			'page': 1,
			'parent': 0,
			'exclude': 1,
		};

		if (typeof Constants.Categories != 'undefined') {
			postData['include'] = Constants.Categories;
		}

		self.setState({isLoading: true});
		Api.getCategories(postData)
			.then(function (data) {
				// console.log('categories data here: ', data);

				if (data != null) {
					self.setState({
						page: self.state.page + 1,
						finish: true,
						isLoading: false,
						hasData: true,
						dataSource: self.getDataSource(data)
					});
				}
			});
	}

	getDataSource(data) {
		return this.state.dataSource.cloneWithRows(data);
	}

	renderRow(category) {
		return <CategoryControl category={category}/>
	}

	render() {
		if(this.state.hasData == false){
				return <Spinkit css={css.loading} isLoading={this.state.isLoading} />;
		}else{
				return (
					<View style={css.body}>
						<Toolbar name={Languages.category}
						         action={Actions.category}
						         />
						<ListView
							contentContainerStyle={ Platform.OS === 'android' ? css.listViewAndroid:css.listView}
							enableEmptySections={true}
							dataSource={this.state.dataSource}
							showsHorizontalScrollIndicator={false}
							renderRow={this.renderRow.bind(this)}>
						</ListView>
					</View>
				);
		}
	}
}
