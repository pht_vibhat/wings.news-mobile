'use strict';
import React, {Component} from "react";
import {Text, View, Image, TouchableOpacity, TextInput, NativeModules} from "react-native";
import css from "./style";
import Spinner from "react-native-spinkit";
import FacebookButton from "@controls/FacebookButton/Index";
import Languages from "@common/Languages";
import AppEventEmitter from "@services/AppEventEmitter";
import User from "@services/User";
import Tools from "@common/Tools";

export default class Signin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			email: '',
			password: ''
		}
	}

	btnLogIn() {
		this.setState({
			loading: true
		});
		User.login(this.state.email.trim(), this.state.password,
			() => {
				console.log('btnLogIn')
				this.setState({loading: false});
				console.log('btnLogIn')
				User.clearPosts();
				Tools.refresh();
			},
			(error) => {
				this.setState({loading: false});
				console.log('btnLogIn')
				AppEventEmitter.emit('login.showInfo', error.message)
			});
	}

	render() {
		return (
			<View style={css.wrap}>
				<View style={css.body}>
					<View style={css.wrapForm}>

						<View style={css.textInputWrap}>
							<Text style={css.textLabel}>{Languages.email}</Text>
							<TextInput
								placeholder={Languages.enterEmail}
								underlineColorAndroid="transparent"
								style={css.textInput}
								onChangeText={(text) => this.setState({email: text})}/>
						</View>

						<View style={css.textInputWrap}>
							<Text style={css.textLabel}>{Languages.passwordUp}</Text>
							<TextInput placeholder={Languages.enterPassword}
							           underlineColorAndroid="transparent"
							           style={css.textInput}
							           secureTextEntry={true}
							           onChangeText={(text) => this.setState({password: text})}/>
						</View>

						<TouchableOpacity onPress={this.forgotPassword}>
							<Text style={css.forgotPassText}>{Languages.forgotPassword}</Text>
						</TouchableOpacity>

					</View>

					<View style={css.wrapButton}>
						{this.state.loading?
							<TouchableOpacity style={css.btnLogIn}>
									<Spinner
											 size={20}
											 type="FadingCircle"
											 color="#FFFFFF"/>
							</TouchableOpacity>
						: <TouchableOpacity style={css.btnLogIn} onPress={this.btnLogIn.bind(this)}>
							<Text style={css.btnLogInText}> {Languages.login} </Text>
						</TouchableOpacity>
					}
						<FacebookButton />
					</View>
				</View>

			</View>
		);
	}
}
