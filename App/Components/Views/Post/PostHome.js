'use strict';

import React, {Component} from "react";
import {Text, View, ScrollView} from "react-native";
import style from "./style";
import PostFeature from "./PostFeature";
import Constants from "@common/Constants";
import Languages from "@common/Languages";

export default class PostHome extends Component {
  render() {
    return (
			<View style={style.hlist}>
				<PostFeature id="kone" layout={Constants.Post.layout_three}/>

				<View>
					<Text style={style.title}>{Languages.feature}</Text>
					<Text style={style.titleSmall}>{Languages.forLife}</Text>
				</View>
				<PostFeature id="ktwo"  tag={Constants.Tags.feature} layout={Constants.Post.layout_two}/>

				<View>
					<Text style={style.title}>{Languages.editorchoice}</Text>
					<Text style={style.titleSmall}>{Languages.powerBy}</Text>
				</View>
				<PostFeature id="kthree"  tag={Constants.Tags.editor} layout={Constants.Post.layout_one}/>

				<View>
					<Text style={style.title}>{Languages.recent}</Text>
					<Text style={style.titleSmall}>{Languages.powerBy}</Text>
				</View>

				<PostFeature id="klist"
										 vertical={true}
										 tag={Constants.Tags.recent}
										 layout={Constants.Post.layout_list}/>
			</View>
    );
  }
}
