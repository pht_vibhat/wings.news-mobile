'use strict';

import React, {Component} from "react";
import {Text, View, Image, ListView, TouchableOpacity} from "react-native";
import {Actions} from "react-native-router-flux";
import TimeAgo from "react-native-timeago";
import Api from "@services/Api";
import PostHome from "./PostHome";
import Banner from "./Banner";
import news from "./style";
import EventEmitter from "@services/AppEventEmitter";
import LinearGradient from "react-native-linear-gradient";
import CommentIcons from "@controls/CommentIcons/Index";
import Tools from "@common/Tools";
import Spinkit from "@controls/Spinkit/Index";
import SwipeCards from "@controls/SwipeCards/Index";
import Constants from '@common/Constants';

export default class PostCategory extends Component {
  constructor(props) {
    super(props);
    this.data = [];

    // this.tags = [];
    this.categoryActive = null;

    this.state = {
      postLayout: Constants.Post.simple_view,
      page: 1,
      data: [],
      limit: 5,
      hasData: false,
      isLoading: true,
      finish: false,
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    }
  }

  componentWillMount() {

    // this.fetchTags();
    EventEmitter.addListener('posts.fetchData', this.fetchPostData.bind(this)); // Register event listener
    EventEmitter.addListener('posts.cards.fetchData', this.fetchPostData.bind(this)); // Register event listener
    EventEmitter.addListener('posts.updateCategory', this.updateCategory.bind(this));
    EventEmitter.addListener('news.changeLayout', this.changeLayout.bind(this));
  }

  changeLayout(layout) {
    this.setState({postLayout: layout});
    this.fetchPostData(true);
  }

  /**
   * fetching Tags data
   */
  fetchTags() {
    var self = this;
    Api.getTags()
      .then((data) => {
        // console.log('getTags', data);
        this.tags = data;
        self.setState({isLoading: false});
        self.fetchPostData();
      });
  }

  /**
   * Update current active category from the menu
   * @param data
   */
  updateCategory(data) {
    if (typeof data != 'undefined') {
      this.categoryActive = data.category;
      this.data = [];
      this.setState({
        page: 1,
        finish: false,
        dataSource: this.getDataSource(this.data)
      });
    }

    // reload the post
    this.fetchPostData(true)
  }

  fetchPostData(isReload) {
    var self = this;

    if (typeof isReload == 'undefined' && (this.state.finish || this.state.isLoading)) {
      return;
    }

    var postData = {
      'per_page': this.state.limit,
      'page': this.state.page,
      'sticky': false
    };

    if (this.categoryActive !== null) {
      postData['categories'] = this.categoryActive;
    }

    if (typeof isReload !== 'undefined') {
      postData.page = 1;
      this.setState({
        page: 1
      });

      // this important to refresh the list view data source to change layout row
      self.data = [];
    }

    self.setState({isLoading: true});

    Api.getPosts(postData)
      .then(function (data) {
        // console.log('posts data', data);

        if (typeof data != 'undefined') {
          self.data = self.data.concat(data);
          self.setState({
            page: self.state.page + 1,
            finish: data.length < self.state.limit,
            isLoading: false,
            data: self.data,
            hasData: true,
            dataSource: self.getDataSource(self.data)
          });
        }
        else {
          self.setState({finish: true});
        }
      });
  }

  /**
   * Get data source for the list view
   * @param posts
   * @returns {*}
   */
  getDataSource(posts) {
    return this.state.dataSource.cloneWithRows(posts);
  }

  viewCategoryDetail(id) {
    Tools.viewCateDetail(id);
  }

  /**
   * Render row for the list view
   * @param post
   * @param sectionID
   * @param rowID
   * @returns {*}
   */
  renderRow(post, sectionID, rowID) {
    if (typeof (post.title) == 'undefined') {
      return null;
    }
    const imageUrl = Tools.getImage(post);

    const authorName = post._embedded.author[0]['name'];

    const commentCount = typeof post._embedded.replies == 'undefined' ? 0 : post._embedded.replies[0].length;

    const postTitle = typeof post.title == 'undefined' ? '' : post.title.rendered;

    const postContent = typeof post.excerpt == 'undefined' ? '' : post.excerpt.rendered;

    if (this.state.postLayout == Constants.Post.simple_view) {
      return (
        <TouchableOpacity style={news.panelList} onPress={Actions.postDetails.bind(this, {post: post})}>
          <Image source={{uri: imageUrl}} style={news.imageList}></Image>
          <View style={news.titleList}>
            <Text style={news.nameList}>{Tools.getDescription(postTitle, 300)}</Text>
            <Text style={news.descriptionList}>{Tools.getDescription(postContent, 120)}</Text>

            <View style={{flexDirection: 'row'}}>
              <TimeAgo style={news.timeList} time={post.date} hideAgo={true} />
              <Text style={news.category}>by {authorName}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )
    }

    // large view layout
    if (this.state.postLayout == Constants.Post.list_view) {
      return (
        <TouchableOpacity elevation={10} style={news.cardNews}
                          onPress={Actions.postDetails.bind(this, {post: post})}>
          <View style={news.cardView}>
            <Image style={news.largeImage} source={{uri: imageUrl}}></Image>

            <LinearGradient style={news.linearGradient} colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.7)"]}>
              <Text style={news.newsTitle}>{Tools.getDescription(postTitle) }</Text>
            </LinearGradient>

            <View style={news.description}>
              <Text style={news.author}><TimeAgo time={post.date} hideAgo={true} /> by @{authorName}</Text>
              <CommentIcons post={post} comment={commentCount} />
            </View>
          </View>
        </TouchableOpacity>
      )
    }

    // list view layout Constants.Post.narrow_view
    return (
      <TouchableOpacity style={news.smCardNews} onPress={Actions.postDetails.bind(this, {post: post})}>
        <View style={news.cardView}>
          <Image style={news.smImage} source={{uri: imageUrl}}></Image>
          <View style={news.smDescription}>
            <Text style={news.smTitle}>{Tools.getDescription(postTitle) }</Text>
            <Text style={news.smAuthor}><TimeAgo time={post.date} hideAgo={true} /> by @{authorName}</Text>
          </View>

          <CommentIcons hideOpenIcon={true} post={post} style={news.smShareIcons} comment={commentCount} />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    // default homepage
    if (this.categoryActive == null && this.state.postLayout == Constants.Post.simple_view) {
      return <View style={news.body}>
        <Banner />
        <PostHome />
      </View>
    }

    // category list view page
    return (
      <View style={news.body}>
        {this.state.hasData && this.state.postLayout == Constants.Post.card_view &&
        <SwipeCards data={this.state.data} /> }

        {this.state.postLayout != Constants.Post.card_view &&
        <ListView
          contentContainerStyle={this.state.postLayout != Constants.Post.simple_view && news.listView}
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow.bind(this)}>
        </ListView> }

        <Spinkit isLoading={this.state.isLoading} />
      </View>
    );
  }
}
