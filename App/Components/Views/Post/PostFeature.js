'use strict';

import React, {Component} from "react";
import {ListView} from "react-native";
import Post from "./Post";
import wp from '@services/WPAPI';
import css from "./style";
import Constants from '@common/Constants';
import InvertibleScrollView from 'react-native-invertible-scroll-view';

export default class PostFeature extends Component {

  constructor(props) {
    super(props);
    this.data = [];

    this.state = {
      limit: 10,
      page: 1,
      refreshing: false,
      isLoading: false,
      categories: null,
      finish: false,
      datasource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    };
  }


  componentDidMount() {
    this.fetchPostData();
  }

  fetchPostData() {
    var self = this;
    // console.log('--start fetching post--');

    if ((this.state.finish || this.state.isLoading)) {
      // console.log('is loading or finish', this.state.finish, this.state.isLoading);
      return;
    }

    self.setState({isLoading: true});
    const api = wp.posts()
      .perPage(this.state.limit)
      .page(this.state.page)
      .sticky(false)
      .embed();
    if (typeof this.props.tag != 'undefined') {
      api.tags(this.props.tag)
    }

    api.get(function (err, data) {
      if (err) {
        console.log(err, this.props.tag);
      }
      self.data = self.data.concat(data);

      self.setState({
        page: self.state.page + 1,
        finish: data.length < self.state.limit,
        isLoading: false,
        datasource: self.getDataSource(self.data)
      });
    });

    // get category
    if (this.props.vertical && this.state.categories == null) {
      wp.categories().then((data) => {
        var categories = [];
        data.map((cate) => {
          categories[cate.id] = cate.name;
        });
        self.setState({categories: categories});
      });
    }
  }

  getDataSource(posts) {
    return this.state.datasource.cloneWithRows(posts);
  }

  renderRow(layout, post) {
    return <Post post={post} categories={this.state.categories} layout={layout} />
  }

  render() {
    return <ListView
      contentContainerStyle={this.props.vertical ? null : css.featureListView}
      renderScrollComponent={props => Constants.RTL ? <InvertibleScrollView {...props} inverted /> :
        <InvertibleScrollView {...props} />}
      key={this.props.id}

      dataSource={this.state.datasource}
      horizontal={this.props.vertical ? false : true}

      showsHorizontalScrollIndicator={this.props.vertical ? true : false}

      onEndReachedThreshold={100}
      onEndReached={this.fetchPostData.bind(this)}

      renderRow={this.renderRow.bind(this, this.props.layout)}>
    </ListView>;
  }
}
