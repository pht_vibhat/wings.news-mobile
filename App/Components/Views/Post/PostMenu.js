import React, {Component} from "react";
import {View, Text, Animated, ScrollView, TouchableOpacity} from "react-native";
import Toolbar from "@controls/Toolbar";
import css from "@common/style";
import Api from "@services/Api";
import EventEmitter from "@services/AppEventEmitter";
import news from "./style";
import {Actions} from "react-native-router-flux";
import Constants from "@common/Constants";
import Languages from "@common/Languages";
import Category from "@services/Category";

export default class PostMenu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			paddingTop: 100,
			categories: null,
			categoryActive: null,
			_animatedMenu: this.props.animateMenu,
			selectedCategories: []
		};
	}

	fetchData() {
		var self = this;
		var postData = {}
		Category.getSettingsCategories().then((data) => {
			if(data !== undefined) {
				let includeArr = [];
				for(let i = 0; i< data.length; i++) {
					includeArr.push(data[i].id)
				}
				var postData = {
					'per_page': 100,
					'page': 1,
					'exclude': 30,
					'include': includeArr
				};
				Api.getCategories(postData).then((data) => {
					//console.log('getCategories', data);
					self.setState({categories: data});
				});

			} else {
				if (this.state.categories != null) {
					return;
				}
				postData = {
					'per_page': 100,
					'page': 1,
					'parent': 0,
					'exclude': 30
				};
				//console.log('post data : ',postData)
			}
			Api.getCategories(postData).then((data) => {
				//console.log('getCategories', data);
				self.setState({categories: data});
			});
		});

	}

	settingsData(selectedCategories) {
		var self = this;
		var postData = {}
			let includeArr = [];
			for(let i = 0; i< selectedCategories.length; i++) {
				includeArr.push(selectedCategories[i].id)
			}
			var postData = {
				'per_page': 100,
				'page': 1,
				'exclude': 30,
				'include': includeArr
			};

		Api.getCategories(postData).then((data) => {
			console.log('getCategories', data);
			self.setState({categories: data});
		});


	}

	componentWillMount() {
		this.fetchData();
		EventEmitter.addListener('setting.selectedCategories', this.settingsData.bind(this));
		EventEmitter.addListener('homepage.setActiveCategory', this.selectTab.bind(this));
	}

	selectTab(categoryId) {
		this.setActiveCategory(categoryId);
	}

	/**
	 * Format the category name
	 * @param string
	 * @returns {string}
	 */
	upCaseTitle(string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase().replace(' &amp;', '');
	}

	/**
	 * Set active category
	 * @param cateId
	 */
	setActiveCategory(cateId) {
		//console.log(cateId);
		// this.categoryActive = cateId;
		this.setState({categoryActive: cateId});
		EventEmitter.emit('posts.updateCategory', {category: cateId});
		EventEmitter.emit('posts.updateBanner', {category: cateId});
	}

	updateLayout() {
		this.setState({oneColumnLayout: !this.state.oneColumnLayout});
	}

	render() {
		if (this.state.categories === null) {
			return <Toolbar css={news.toolbar} action={Actions.news} listViewButton={true} newsLayoutButton={true} cardButton={true} listButton={true} />
		}

		return <Animated.View style={[css.toolbarView, {transform: [{translateY: this.state._animatedMenu}]}]}>
			<Toolbar css={news.toolbar}  action={Actions.news} listViewButton={true}  newsLayoutButton={true} cardButton={true} listButton={true} />

			<View style={news.menuView}>
				<ScrollView directionalLockEnabled={true}
				            showsHorizontalScrollIndicator={false}
				            horizontal={true}>
					<TouchableOpacity
						onPress={ this.setActiveCategory.bind(this, null) }
						style={[news.menuItemView, this.state.categoryActive == null ? news.menuItemActive : null]}>
						<Text
							style={[news.menuItem, this.state.categoryActive == null ? news.menuActiveText : null]}>{Languages.all}</Text>
					</TouchableOpacity>

					{typeof this.state.categories != 'undefine' ?
					 this.state.categories.map((category, i) => {
						//console.log('Categories at post menu : ',categories);
						return (
							<TouchableOpacity
								onPress={this.setActiveCategory.bind(this, category.id) }
								key={category.id}
								style={[news.menuItemView, this.state.categoryActive == category.id ? news.menuItemActive : null]}>
								<Text
									style={[news.menuItem, this.state.categoryActive == category.id ? news.menuActiveText : null]}>
									{this.upCaseTitle(category.slug) }
								</Text>
							</TouchableOpacity>)
					}) : <View />}

				</ScrollView>
			</View>
		</Animated.View>
	}
}
