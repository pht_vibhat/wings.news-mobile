'use strict';

import React, {Component} from "react";
import {StatusBar} from "react-native";
import PostCategory from "./PostCategory";
import StickyScrollView from "./StickyScrollView";

export default class Index extends Component {
	constructor(props) {
		super(props);
		StatusBar.setHidden(true);
	}

	render() {
		return (
			<StickyScrollView>
				<PostCategory />
			</StickyScrollView>
		);
	}
}
