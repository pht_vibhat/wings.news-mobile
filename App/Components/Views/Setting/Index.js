'use strict';

import React, {Component, PropTypes} from "react";
import AppEventEmitter from "@services/AppEventEmitter";
import Tools from "@common/Tools";
import {StatusBar, Text, View, ScrollView, Picker, TouchableOpacity, ListView, Alert, Dimensions, Image, Platform} from "react-native";
const {width, height, scale} = Dimensions.get("window");
// import Modal from 'react-native-modal';
import InvertibleScrollView from 'react-native-invertible-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons'
import Slider from "@controls/Slider/Index";
import css from "./style";
import {Actions} from "react-native-router-flux";
import Toolbar from "@controls/Toolbar";
import Languages from "@common/Languages";
import SelectMultiple from 'react-native-select-multiple';
import Constants from "@common/Constants";
import Api from "@services/Api";
import Category from "@services/Category";
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class Setting extends Component {
	constructor() {
		super();

		this.state = {
			categories: ds.cloneWithRows(['row 1', 'row 2']),
			countries: ds.cloneWithRows(['row 1', 'row 2']),
			selectedlanguage: 'India(English)',
			selectedItems: [],
			selectedCountry: []
		};
	}

	fetchData() {
		var self = this;
		//init categories
		var postData = {
			'per_page': 100,
			'page': 1,
			'parent': 0 ,
			'exclude': 30,
		};

		if (typeof Constants.Categories != 'undefined')	{
			postData['include'] = Constants.Categories;
		}
		let catArray = [];
		Api.getCategories(postData).then((data) => {

			for(let i = 0; i< data.length; i++) {
				let obj =  data[i];
				catArray.push(obj);
			}
			if(this.state.selectedItems.length) {
				self.setState({
					categories: ds.cloneWithRows(catArray),
				});
			} else {
				self.setState({
					categories: ds.cloneWithRows(catArray),
					selectedItems: catArray
				});
			}
		});

	}

	fetchPersistData() {
		Category.getSettingsCategories().then((data) => {
			if(data.length) {
				let selCategories = data.filter((category) => category.parent === 0)
				let selCountry = data.filter((cat) => cat.parent === 6)
				this.setState({
					selectedItems: selCategories,
					selectedCountry: selCountry
				})
			}
		});
	}

	fetchCountryData() {
		var self = this;
		//init categories
		var postData = {
			'per_page': 100,
			'page': 1,
			'parent': 6 ,
		};

		if (typeof Constants.Categories != 'undefined')	{
			postData['include'] = Constants.Categories;
		}
		let catArray = [];
		Api.getCategories(postData).then((data) => {
			for(let i = 0; i< data.length; i++) {
				let obj =  data[i]
				catArray.push(obj)
			}
			if(this.state.selectedCountry.length) {
				self.setState({
					countries: ds.cloneWithRows(catArray),
				});
			} else {
				self.setState({
					countries: ds.cloneWithRows(catArray),
					selectedCountry: []
				});
			}
			// Category.postSettingsCategories(catArray);
			// self.setState({countries: ds.cloneWithRows(catArray)});
		});
	}

	selectLanguage(lang) {
		this.setState({
			selectedlanguage: lang
		})
	}

	componentWillMount() {
		this.fetchData();
		this.fetchCountryData();
		this.fetchPersistData();
	}

	componentDidMount() {
		AppEventEmitter.addListener('setting.edition', this.selectLanguage.bind(this));
	}

	backButtonClicked() {
		// console.log('selectedItems : ************** ',this.state.selectedItems)
		// console.log('selectedCountry : ************** ',this.state.selectedCountry)
		let data = [];
		for(let i= 0;i< this.state.selectedItems.length; i++) {
			data.push(this.state.selectedItems[i])
		}
		for(let i= 0;i< this.state.selectedCountry.length; i++) {
			data.push(this.state.selectedCountry[i])
		}

		Category.postSettingsCategories(data);
		//console.log('data : ************** ',data)
		AppEventEmitter.emit('setting.selectedCategories', data);
		//Actions.home({type: 'reset'});
		 Actions.pop();
	}

	selectedCategories = (rowData) => {
		const { selectedItems } = this.state;
		let flag = []
		if(selectedItems.length) {
			flag = selectedItems.filter((category) => category.id === rowData.id)
		}
		let newArray = [];
		if(flag.length) {
			newArray = selectedItems.filter((category) => category.id !== rowData.id)
		} else {
			newArray = selectedItems;
			newArray.push(rowData)
			//console.log('false : ',newArray);
		}
		this.setState({
			selectedItems: newArray
		})
	}


	selectedCountries = (rowData) => {
		const { selectedCountry } = this.state;
		let flag = []

		if(selectedCountry.length) {
			flag = selectedCountry.filter((country) => country.id === rowData.id)
		}
		let newArray = [];
		if(flag.length) {
			newArray = selectedCountry.filter((country) => country.id !== rowData.id)
		} else {
			newArray = selectedCountry;
			newArray.push(rowData)
		}
		this.setState({
			selectedCountry: newArray
		})
	}




	renderCategoriesRow = (rowData) => {
		const { selectedItems } = this.state;
		const imageURL =  require("@images/category/though.png");
		let flag = []
		if(selectedItems.length) {
			flag = this.state.selectedItems.filter((category) => category.id === rowData.id)
		}

		return (
			<View style={{
				width: (width/3), height: (width/3+50), justifyContent: 'center', backgroundColor: 'transparent',
				marginRight: 10, flexDirection: 'column',
			}}>
				<View style={{ flex: 4, backgroundColor: 'white', borderRadius: 5 }}>
					<View style={{flex: 3}}>
						<Image style={{width: (width/3), height: (width/4), resizeMode: 'contain'}} source={imageURL}/>
					</View>
					<View style={{flex: 1, justifyContent: 'center', alignItems: 'center', borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
						<Text style={{ fontSize: 15, color: '#333', fontWeight: '500' }}>{rowData.name}</Text>
					</View>

				</View>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5}}>
					<TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', backgroundColor: '#f0f0f0', height: 30, width: 60, borderRadius: 20}}
					onPress={this.selectedCategories.bind(this, rowData)}
					>
						<Icon
							name={flag.length === 1 ? 'md-checkmark' : 'md-add'}
							style={{fontSize: 25, color: 'red'}}
						/>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	renderCountriesRow = (rowData) => {
		const { selectedCountry } = this.state;
		const imageURL =  require("@images/category/though.png");
		let flag = []
		if(selectedCountry.length) {
			flag = this.state.selectedCountry.filter((category) => category.id === rowData.id)
		}

		return (
			<View style={{
				width: (width/3), height: (width/3+50), justifyContent: 'center', backgroundColor: 'transparent',
				marginRight: 10, flexDirection: 'column',
			}}>
				<View style={{ flex: 4, backgroundColor: 'white', borderRadius: 5 }}>
					<View style={{flex: 3}}>
						<Image style={{width: (width/3), height: (width/4), resizeMode: 'contain'}} source={imageURL}/>
					</View>
					<View style={{flex: 1, justifyContent: 'center', alignItems: 'center', borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
						<Text style={{ fontSize: 15, color: '#333', fontWeight: '500' }}>{rowData.name}</Text>
					</View>

				</View>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 5}}>
					<TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', backgroundColor: '#f0f0f0', height: 30, width: 60, borderRadius: 20}}
					                  onPress={this.selectedCountries.bind(this, rowData)}
					>
						<Icon
							name={flag.length === 1 ? 'md-checkmark' : 'md-add'}
							style={{fontSize: 25, color: 'red'}}
						/>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	showLanguageModal = () => {
		Actions.edition();
	}

	render() {
		const { categories, countries, selectedlanguage } = this.state
		return (
			<View style={css.wrap}>
				<Toolbar name="Setting"
				         isChild={true}
				         css={css.toolbar}
				         action={this.backButtonClicked.bind(this)}/>
				<ScrollView style={{marginTop: Platform.OS === 'ios' ? 60 : 0}}>
				<View>
					<TouchableOpacity style={css.doneButton} onPress={this.showLanguageModal}>
						<Text style={[css.HeadingTextStyle]}>Edition:</Text>
						<Text style={[css.descTitleStyle]}>{selectedlanguage}</Text>
					</TouchableOpacity>
				</View>

				<View style={[css.boxSetting]}>
					<Text style={[css.HeadingTextStyle]}>Categories: </Text>
					<ListView
						style={{marginTop: 20}}
						renderScrollComponent={props => Constants.RTL ? <InvertibleScrollView {...props} inverted /> :
							<InvertibleScrollView {...props} />}
						dataSource={this.state.categories}
						horizontal={true}
						showsHorizontalScrollIndicator={false}
						onEndReachedThreshold={100}
						renderRow={this.renderCategoriesRow}>
					</ListView>
				</View>
				<View style={[css.boxSetting]}>
					<Text style={[css.HeadingTextStyle]}>Countries: </Text>
					<ListView
						style={{marginTop: 20}}
						renderScrollComponent={props => Constants.RTL ? <InvertibleScrollView {...props} inverted /> :
							<InvertibleScrollView {...props} />}
						dataSource={this.state.countries}
						horizontal={true}
						showsHorizontalScrollIndicator={false}
						onEndReachedThreshold={100}
						renderRow={this.renderCountriesRow}>
					</ListView>
				</View>
				</ScrollView>
			</View>
		);
	}
}
