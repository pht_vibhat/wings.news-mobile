'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import TimeAgo from "react-native-timeago";
import {Actions} from "react-native-router-flux";
import css from "./style";
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Tools from "@common/Tools";
import CommentIcons from "@controls/CommentIcons/Index";
import Constants from '@common/Constants';

export default class VideoControl extends Component {
	constructor(props) {
		super(props);
	}

	viewVideo() {
		Actions.videoDetail({video: this.props.video});
	}

	render() {
		const data = this.props.video;
		const imageURL = Tools.getImage(data);

		return (
			<View style={css.boxShadow} >
				<View style={css.box}>
					<TouchableOpacity onPress={this.viewVideo.bind(this)}>
						<Image source={{uri: imageURL}} style={css.imageBox}>
							<View style={css.iconVideo}>
								<Icon name="control-play" size={25} style={css.iconPlay}>
								</Icon>
							</View>
							<View style={css.overlayVideo}>
							</View>
						</Image>
					</TouchableOpacity>

					<View style={css.boxName}>
						<TouchableOpacity>
							<Text style={css.title}>{Tools.getDescription(data.title.rendered, 300)}</Text>
						</TouchableOpacity>
						<View>
							<Text style={css.time}><TimeAgo time={data.date} hideAgo={true}/></Text>
						</View>

						<CommentIcons post={this.props.video} hideCommentIcon={true} style={ Constants.RTL ? css.shareIcon: null} />
					</View>
				</View>
			</View>
		);
	}
}
