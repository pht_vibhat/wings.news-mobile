'use strict';
import React, {Component} from "react";
import {Text, ListView, View, Platform} from "react-native";
import Api from "@services/Api";
import css from "./style"
import VideoControl from "./VideoControl";
import Constants from "@common/Constants";
import Spinkit from "@controls/Spinkit/Index";

export default class ListVideo extends Component {
	constructor(props) {
		super(props);
		this.data = [];

		this.state = {
			limit: 10,
			page: 1,
			isLoading: false,
			finish: false,
			hasData: false,
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		};
	}

	componentDidMount() {
		this.fetchPostData();
	}

	fetchPostData() {
		var self = this;
		if ((this.state.finish || this.state.isLoading)) {
			return;
		}

		let postData = {
			'per_page': this.state.limit,
			'page': this.state.page,
			'categories': Constants.CategoryVideo,
			'sticky': false
		};

		self.setState({isLoading: true});

		Api.getPosts(postData)
			.then(function (data) {
				//console.log('posts data here: ', data);
				if (data != "") {
					self.data = self.data.concat(data);
					self.setState({
						page: self.state.page + 1,
						finish: data.length < self.state.limit,
						isLoading: true,
						hasData: true,
						dataSource: self.getDataSource(self.data)
					});
				}
			});
	}

	getDataSource(posts) {
		return this.state.dataSource.cloneWithRows(posts);
	}

	renderRow(video) {
		return <VideoControl video={video}/>
	}

	render() {

		if(this.state.hasData == false){
			return (
				<Spinkit isLoading={this.state.isLoading} css={css.loading} />
			);
		}else{
				return (
					<ListView
						style={Platform.OS === 'android' ? css.listAndroid:css.list}
						enableEmptySections={true}
						dataSource={this.state.dataSource}
						onEndReachedThreshold={200}
						onEndReached={this.fetchPostData.bind(this)}
						renderRow={this.renderRow.bind(this)}>
					</ListView>
				);
		}
	}
}
