'use strict';
import React, {Component} from "react";
import {View, StatusBar} from "react-native";
import {Actions} from "react-native-router-flux";
import Toolbar from "@controls/Toolbar";
import css from "./style";
import ListVideo from "@videos/ListVideo";
import Languages from "@common/Languages";

export default class Videos extends Component {
	constructor(props) {
		super(props);
		StatusBar.setHidden(true);
	}

	render() {
		return (
			<View style={css.body}>
				<Toolbar name={Languages.video} action={Actions.videos}/>
				<ListVideo />
			</View>
		);
	}
}
