'use strict';
import React, {Component} from "react";
import {Text, Platform, View, Image, Dimensions, AsyncStorage, WebView as TestWebView} from "react-native";
import Constants  from "@common/Constants";
import TimeAgo from "react-native-timeago";
import {Actions} from "react-native-router-flux";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import LinearGradient from "react-native-linear-gradient";
import css from "@postdetail/style";
import news from "@post/style";
import Tools from "@common/Tools";

import Toolbar from "@controls/Toolbar";
import CommentIcons from "@controls/CommentIcons/Index";
import AdMob from "@controls/AdMob/Index";
import Comment from "@controls/Comment/Index";
import WebView from "@controls/WebView/Index";
import RelatedPost from "@relatedpost/Index";
import Languages from "@common/Languages";
import VideoDetail from "@videodetail/Index";
const {width, height, scale} = Dimensions.get("window");

export default class NewsDetail extends Component {

	constructor(props) {
		super(props);
		this.state = {
			fontSize: Constants.fontText.size
		};

		Tools.getFontSizePostDetail().then((data) => {
			this.setState({fontSize: data})
		})
	}

  render() {
    const post = this.props.post;
    const videoLink = Tools.getLinkVideo(post.content.rendered);

    if(videoLink.length > 22)	{
      return <VideoDetail video={post} />
    }
		const imageURL = Tools.getImage(post);
		const authorName = post._embedded.author[0]['name'];
		const commentCount = typeof post._embedded.replies == 'undefined' ? 0 : post._embedded.replies[0].length;
		const postComments = typeof post._embedded.replies == 'undefined' ? null : post._embedded.replies[0];
		const postTitle = typeof this.props.post.title.rendered == 'undefined' ? '' : this.props.post.title.rendered;
		const postContent = typeof this.props.post.content.rendered == 'undefined' ? '' : this.props.post.content.rendered;
	  //console.log('Images : ',imageURL)
		return (
			<View style={css.color}>
				<Toolbar name="" action={Actions.pop} isChild="true"/>

				<ParallaxScrollView
					backgroundColor="white"
					contentBackgroundColor="white"
					parallaxHeaderHeight={380}

					renderBackground={() => (
						<View style={Platform.OS !== 'android' && {paddingTop: 50}}>
							<Image style={css.imageBackGround}
							       source={{uri: imageURL}}>
								<Image source={Constants.waterMark} style={css.watermarkStyle} />
							</Image>
						</View>
					)}
					renderForeground={() => (
						<View style={css.detailPanel}>
							<LinearGradient style={css.linearGradient} colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.8)"]}>
								<Text style={css.detailDesc}>{Tools.getDescription(postTitle, 300) }
								</Text>
							</LinearGradient>
						</View>
					)}>

          <View style={{flex: 0, minHeight: 800}} >
      				<View style={news.description}>
      					<Text style={css.author}><TimeAgo time={post.date} hideAgo={true}/> {Languages.by} @{authorName}
      					</Text>

      					<CommentIcons post={this.props.post} comment={commentCount} />

      				</View>

						<WebView html={postContent}/>

              {/*You can enable the AdMob here but i suggest no as it is having performance issue
    					 <AdMob />*/}



	          <Comment post={this.props.post}
	                   comments={typeof post._embedded.replies == 'undefined' ? null : post._embedded.replies[0]}/>
    					<RelatedPost post={this.props.post}/>

          </View>

				</ParallaxScrollView>
			</View>
		);
	}
}
