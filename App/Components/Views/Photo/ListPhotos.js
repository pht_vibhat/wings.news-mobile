import React, {Component} from "react";
import {View, ListView, Platform} from "react-native";
import Api from "@services/Api";
import css from "@photo/style";
import Photo from "./Photo";
import Spinkit from "@controls/Spinkit/Index";
import Constants from "@common/Constants";

export default class ListPhotos extends Component {
	constructor(props) {
		super(props);
		this.data = [];
		this.state = {
			limit: 10,
			page: 1,
			isLoading: false,
			finish: false,
			hasData: false,
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		};
	}

	componentDidMount() {
		this.fetchPostData();
	}

	fetchPostData(isReload) {
		var self = this;

		if (this.state.finish || this.state.isLoading) {
			return;
		}
		// console.log('page at fetch posts : ', this.state.page);
		let postData = {
			'per_page': this.state.limit,
			'page': this.state.page,
			'filters[tag]': Constants.Tags.photo
		};
		if (typeof isReload === 'undefined') {
			postData.page = this.state.page;
			// console.log('is releoad called', isReload)
		}

		self.setState({isLoading: true});

		Api.getPosts(postData)
			.then(function (data) {
				if (data != null) {
					self.data = self.data.concat(data);
					self.setState({
						page: self.state.page + 1,
						finish: data.length < self.state.limit,
						isLoading: false,
						hasData: true,
						dataSource: self.getDataSource(self.data)
					});
					// console.log('pages : ', self.state.page);
				}
			});
	}

	getDataSource(posts) {
		return this.state.dataSource.cloneWithRows(posts);
	}

	renderRow(photo) {
		return <Photo photo={photo}/>
	}

	render() {
		if(this.state.hasData == false){
				return <Spinkit css={css.loading} isLoading={this.state.isLoading} />;
		}else{
			return (
					<ListView
						contentContainerStyle={Platform.OS === 'android' ? css.listViewAndroid:css.listView}
						enableEmptySections={true}
						dataSource={this.state.dataSource}

						onEndReachedThreshold={400}
						onEndReached={this.fetchPostData.bind(this)}

						renderRow={this.renderRow.bind(this)}>
					</ListView>
			);
		}
	}
}
