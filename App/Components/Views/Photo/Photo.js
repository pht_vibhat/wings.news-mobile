import React, {Component} from "react";
import {View, TouchableHighlight, Image} from "react-native";
import css from "./style";
import AppEventEmitter from "@services/AppEventEmitter";
import CommentIcons from "@controls/CommentIcons/Index";
import LinearGradient from "react-native-linear-gradient";
import Tools from "@common/Tools";
import Constants from '@common/Constants';

export default class Photo extends Component {
	constructor(props) {
		super(props);
	}

	openPhoto(data) {
		AppEventEmitter.emit('open.photo.click', data);
	}

	render() {
		const data = this.props.photo;
		const commentCount = typeof data._embedded.replies == 'undefined' ? 0 : data._embedded.replies[0].length;
		const imageURL = Tools.getImage(data);
		// console.log('get url image : ',imageURL);
		// console.log('data : ',data)

		return (
			<View style={css.boxPhoto}>
				<TouchableHighlight
					underlayColor="white"
					style={css.boxWrapImage}
					onPress={this.openPhoto.bind(this, data)}>
					<Image source={{uri: imageURL}} style={css.boxImage}/>
				</TouchableHighlight>

				<LinearGradient style={css.linearGradient} colors={["rgba(0,0,0,0)", "rgba(0,0,0, 0.3)"]}>
					<CommentIcons post={this.props.photo} style={Constants.RTL ? css.shareIcons : null} hideOpenIcon={true} hideCommentIcon={true} color='#FFFFFF'/>
				</LinearGradient>
			</View>
		);
	}
}
