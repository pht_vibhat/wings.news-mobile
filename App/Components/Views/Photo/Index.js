import React, {Component} from "react";
import {View, Text, Image, TouchableOpacity} from "react-native";
import {Actions} from "react-native-router-flux";
import css from "@photo/style";
import Toolbar from "@controls/Toolbar";
import ListPhotos from "@photo/ListPhotos";
import PhotoModal from "./PhotoModal";
import Languages from "@common/Languages";

export default class Photo extends Component {
	render() {
		return (
			<View style={css.body}>
				<Toolbar name={Languages.photo} action={Actions.photos}/>
				<ListPhotos />
				<PhotoModal />
			</View>
		);
	}
}
