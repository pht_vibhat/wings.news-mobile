import React, {Component} from "react";
import {View, Text, Image, TouchableOpacity} from "react-native";
import css from "./style";
import AppEventEmitter from "@services/AppEventEmitter";
import Tools from "@common/Tools";
import Modal from "react-native-modalbox";

export default class PhotoModal extends Component {
	constructor(props) {
		super(props);
		this.state = {gallery: null}
	}

	componentWillMount() {
		AppEventEmitter.addListener('open.photo.click', this.openPhoto.bind(this));
		AppEventEmitter.addListener('close.photo.click', this.closePhoto.bind(this));
	}

	openPhoto(data) {
		this.refs.modalPhoto.open();

		const imageFeature = Tools.getImage(data);
		var galleryImages = [];

		// we can use the custom field & API here, remove for this scope
		//typeof (data.custom_fields) != 'undefined' ? data.custom_fields.gallery_images : [];
		galleryImages.push({url: imageFeature});

		// console.log(galleryImages);

		this.setState({gallery: galleryImages});
	}

	closePhoto() {
		this.refs.modalPhoto.close();
	}

	renderImages(gallery) {
		// notice: the 1st solution only works if the URI is using https, or by setting the App Transport Security.
		if (gallery) {
			return gallery.map((element, idx) =>
				<Image key={idx}
				       source={{uri: element.url}}
				       style={css.image}/>
			);
		}
	}

	render() {
		return (
			<Modal ref="modalPhoto" swipeToClose={false} animationDuration={200} style={css.modalBoxWrap}>
				{this.renderImages(this.state.gallery)}
				<TouchableOpacity style={css.iconZoom} onPress={this.closePhoto.bind(this)}>
					<Image source={require('@images/icon-zoom-in.png')}
					       style={[css.imageIcon, {top: 2, right: 4}]}></Image>
				</TouchableOpacity>
			</Modal>
		);
	}
}
