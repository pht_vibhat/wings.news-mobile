'use strict';

import React, {Component} from "react";
import {View, ListView, ScrollView, TextInput, Image, Text} from "react-native";
import {Actions} from "react-native-router-flux";
import css from './style';
import Toolbar from "@controls/Toolbar";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import Api from "@services/Api";
import WebView from "@webview/WebView";

export default class CustomPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			html: ''
		}
	}

	componentWillMount() {
		this.fetchPostData();
	}

	fetchPostData() {
		var self = this;
		var pageData = {
			id: self.props.id
		}

		Api.getPages(pageData)
			.then((data) => {
				// console.log(data);
				self.setState({html: typeof data.content.rendered != 'undefined' ? data.content.rendered : "Content is updating"})
			});
	}

	render() {
		return (
			<ScrollView>
				<Toolbar name={this.props.title} css={{opacity: 0.9}} action={Actions.pop}/>
				<WebView html={this.state.html}/>
			</ScrollView>
		)
	}
}
