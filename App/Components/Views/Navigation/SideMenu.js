'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image, ScrollView} from "react-native";
import css from "./style";
import {Actions} from "react-native-router-flux";
import Languages from "@common/Languages";
import User from "@services/User";
import Tools from "@common/Tools";
import Constants from "@common/Constants";
import EventEmitter from "@services/AppEventEmitter";

export default class SideMenu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userData: ''
		};
		this.fetchDataUser();

		EventEmitter.addListener('sidemenu.refresh', this.fetchDataUser.bind(this));
	}

	fetchDataUser() {
		var self = this;
		User.getUser().then((data) => {
			self.setState({
				userData: data
			});
		});
	}

	toCusTomPageContact() {
		Actions.custompage({id: Constants.CustomPages.contact_id, title: Languages.contact});
	}

	toCusTomPageAboutUs() {
		Actions.custompage({id: Constants.CustomPages.aboutus_id, title: Languages.aboutus});
	}

	logout() {
		User.logOut();
		Tools.refresh();
	}

	login() {
		Actions.login();
	}
	settings() {
		Actions.setting()
	}

	render() {
		return (
			<ScrollView>
				<View style={[css.sideMenu, this.props.menuBody]}>
					{this.state.userData ?
						<View style={css.profile}>
							<View style={css.avatarView}>
								<Image style={css.avatar} source={{uri: this.state.userData.photoURL ? this.state.userData.photoURL : 'https://freeiconshop.com/files/edd/person-flat.png'}}/>
							</View>
							
							<Text style={[css.email, this.props.textColor]}>{this.state.userData.displayName != null ? this.state.userData.displayName : this.state.userData.email}</Text>
						</View> : null}

					<TouchableOpacity
						style={[css.menuRow, this.props.rowStyle]}
						underlayColor="#2D2D30"
						onPress={Actions.home}>
						<Text style={[css.menuLink, this.props.textColor]}>{Languages.news}</Text>
					</TouchableOpacity>

					{/*<TouchableOpacity*/}
						{/*style={[css.menuRow, this.props.rowStyle]}*/}
						{/*underlayColor="#2D2D30"*/}
						{/*onPress={this.toCusTomPageContact.bind(this)}>*/}
						{/*<Text style={[css.menuLink, this.props.textColor]}>{Languages.contact}</Text>*/}
					{/*</TouchableOpacity>*/}

					{/*<TouchableOpacity*/}
						{/*style={[css.menuRow, this.props.rowStyle]}*/}
						{/*underlayColor="#2D2D30"*/}
						{/*onPress={this.toCusTomPageAboutUs.bind(this)}>*/}
						{/*<Text style={[css.menuLink, this.props.textColor]}>{Languages.aboutus}</Text>*/}
					{/*</TouchableOpacity>*/}

					<TouchableOpacity
						style={[css.menuRow, this.props.rowStyle]}
						underlayColor="#2D2D30"
						onPress={this.settings.bind(this)}>
						<Text style={[css.menuLink, this.props.textColor]}>{Languages.setting}</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[css.menuRowLogout, this.props.rowStyle]}
						underlayColor="#2D2D30"
						onPress={this.logout.bind(this)}>
						<Text style={[css.logoutLink, this.props.textColor]}>{Languages.logout}</Text>
					</TouchableOpacity>

				</View>
			</ScrollView>
		);
	}
}
