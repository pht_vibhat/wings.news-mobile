'use strict';

import React, {Component} from "react";
import {Text, View, Image, ListView, TouchableOpacity, ScrollView} from "react-native";
import css from "./style";
import AppEventEmitter from "@services/AppEventEmitter";
import Toolbar from "@controls/Toolbar";
import PostReadLater from "./PostReadLater";
import User from "@services/User";
import CardView from "./CardView";
import Spinkit from "@controls/Spinkit/Index";
import Languages from "@common/Languages";

export default class ReadLater extends Component {
	constructor(props) {
		super(props);
		this.data = [];
		this.state = {
			isLoading: true,
			cardView: false,
			dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		};

		AppEventEmitter.addListener('readLater.update', this.fetchPostData.bind(this));
		AppEventEmitter.addListener('readlater.changeLayout', this.changeLayout.bind(this));
	}

	changeLayout() {
		this.setState({cardView: !this.state.cardView})
	}

	componentWillMount() {
		this.fetchPostData();
	}

	async fetchPostData() {
		const self = this;
		const data = await User.getPosts();
		self.data = data;
		if (this.refs.listview) {
			self.setState({dataSource: self.getDataSource(data)});
			this.setState({isLoading: false});
		}
	}

	getDataSource(posts) {
		return this.state.dataSource.cloneWithRows(posts);
	}

	clearAll() {
		const self = this;
		User.clearPosts(true).then(function () {
			self.setState({dataSource: self.getDataSource([])});
		})
	}

	renderRow(post) {
		if (post != null) {
			return <PostReadLater post={post}/>
		}
		return null;
	}

	render() {
		if (this.state.cardView) {
			return <CardView ref="cardview" data={this.data}/>
		}

		return (
			<View style={css.body}>
				<Toolbar css={css.toolbarMenu} name={Languages.readlater} newsLayoutButton={true}/>

				<View style={css.topBar}>
					<TouchableOpacity onPress={this.clearAll.bind(this)}><Text
						style={css.clearText}>{Languages.clear}</Text></TouchableOpacity>
				</View>
				<Spinkit isLoading={this.state.isLoading} />
				<ListView
					ref="listview"
					contentContainerStyle={css.listView}
					enableEmptySections={true}
					horizontal={false}
					showsHorizontalScrollIndicator={false}
					renderRow={this.renderRow.bind(this)}
					dataSource={this.state.dataSource}/>
			</View>
		)
	}
}
