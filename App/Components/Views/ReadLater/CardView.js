'use strict';

import React, {Component} from "react";
import {Dimensions, View} from "react-native";
import css from "./style";
import Card from "./Card";
import Toolbar from "@controls/Toolbar";
import Carousel from 'react-native-snap-carousel';

const {width, height, scale} = Dimensions.get("window"),
	vw = width / 100,
	vh = height / 100;

export default class CardView extends Component {
	renderCard(post, index) {
		if (post != null) {
			return <Card post={post}/>
		}
		return null;
	}

	render() {
		return (
			<View style={css.body}>
				<Toolbar name="Read later" newsLayoutButton={true}/>
				<Carousel
					ref={'carousel'}
					items={this.props.data}
					renderItem={this.renderCard}
					sliderWidth={vw * 75}
					itemWidth={(vw * 86)}
					slideStyle={css.slide}/>
			</View>
		)
	}
}
