'use strict';

import React, {Component} from "react";
import {Text, View, TouchableOpacity, Image} from "react-native";
import TimeAgo from "react-native-timeago";
import {Actions} from "react-native-router-flux";
import Swipeout from '@custom/react-native-swipeout';
import User from '@services/User';

import Tools from "@common/Tools";
import css from "./style";

export default class PostReadLater extends Component {
	constructor(props) {
		super(props);
		this.state = {isRemove: false}
	};

	viewPost() {
		Actions.postDetails({post: this.props.post});
	}

	removePost(post) {
		User.removePost(post);
		this.setState({isRemove: true});
	}

	render() {
		if (this.state.isRemove)    {
			return null;
		}

		const swipeBtns = [{
			text: 'Delete',
			backgroundColor: '#E3222C',
			borderColor: '#fff',
			borderWidth: '2',
			underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
			onPress: () => {
				this.removePost(this.props.post)
			}
		}];

		const data = this.props.post;
		const imageURL = Tools.getImage(data);
		const postTitle = typeof data.title.rendered != 'undefined' ? data.title.rendered : '';

		return (
			<Swipeout style={ {backgroundColor: '#fff'} } right={swipeBtns} >
				<TouchableOpacity style={css.panel} onPress={this.viewPost.bind(this)}>
					<Image source={{uri: imageURL}} style={css.image}></Image>

					<View style={css.title}>
						<Text style={css.name}>{Tools.getDescription(postTitle, 150)}</Text>
						<Text style={css.time}><TimeAgo time={data.date} hideAgo={true}/></Text>
					</View>
				</TouchableOpacity>
			</Swipeout>
		);
	}
}
