import React, { Component } from 'react';
import { View, Text, ListView, TouchableOpacity, Platform } from 'react-native';
import Toolbar from "@controls/Toolbar";
import css from "./styles";
import {Actions} from "react-native-router-flux";
import Icon from 'react-native-vector-icons/Ionicons'
import AppEventEmitter from "@services/AppEventEmitter"
const listData = [
	'India(English)',
	'USA(English)',
	'Deutschaland(Deutsch)',
	'America latina(Español)',
	'Australia(English)',
	'Brasil(Português Brasil)',
	'Canada(English)',
	'Česká Republika(Čeština)'
]
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class Edition extends Component {

	constructor() {
		super();

		this.state = {
			languagesData: ds.cloneWithRows(listData),
			selectedlanguage: listData[0]
		};
	}

	backButtonClicked() {
		Actions.pop();
	}

	selectLanguage = (rowData) => {
		AppEventEmitter.emit('setting.edition', rowData);
		this.setState({
			selectedlanguage: rowData,
			languagesData: ds.cloneWithRows(listData),
		})
		// Actions.pop();
	}


	renderCategoriesRow = (rowData) => {
		return (
			<TouchableOpacity
				style={{height: 50, justifyContent: 'center', flexDirection: 'row'}}
			  onPress={this.selectLanguage.bind(this, rowData)}
			>
				<View style={{ flex: 3, alignItems: 'flex-start', marginRight: 20, justifyContent: 'center' }}>
					<Text style={{ fontSize: 15, color: '#333' }}>{rowData}</Text>
				</View>
				<View style={{ flex: 1, alignItems: 'flex-end', marginRight: 20, justifyContent: 'center' }}>
					<Icon
						name={this.state.selectedlanguage === rowData ? 'md-radio-button-on' : 'md-radio-button-off'}
						style={{fontSize: 25, color: '#333'}}
					/>
				</View>
			</TouchableOpacity>
		)
	}

	renderSeparator = () => {
		return (
			<View style={{height:1, backgroundColor: 'red'}}/>
		);
	}

	render() {
		return (
			<View style={{flex: 1}}>
				<Toolbar name="Edition"
				         isChild={true}
				         css={css.toolbar}
				         action={this.backButtonClicked.bind(this)}/>
				<ListView
					style={[css.boxSetting, {marginTop: Platform.OS === 'ios' ? 60 : 0}]}
					dataSource={this.state.languagesData}
					renderRow={this.renderCategoriesRow}
				/>


			</View>
		)
	}
}
