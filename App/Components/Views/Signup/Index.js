'use strict';
import React, {Component} from "react";
import {Text, View, Image, TouchableOpacity, TextInput} from "react-native";
import css from "@signin/style";
import Languages from "@common/Languages";
import Spinner from "react-native-spinkit";
import User from "@services/User";
import Tools from "@common/Tools";
import AppEventEmitter from "@services/AppEventEmitter";

export default class SignUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			email: '',
			password: '',
			name: ''
		}
	}

	btnSignUp() {
		this.setState({
			loading: true
		});

		User.create(this.state.email, this.state.password, this.state.name,
			() => {
				this.setState({loading: false});

				User.clearPosts();
				Tools.refresh();
		},
			(error) => {
				this.setState({loading: false});
				if (typeof error.message != 'undefined')    {
					AppEventEmitter.emit('login.showError', error.message);
				}
				else if (typeof error.error != 'undefined') {
					AppEventEmitter.emit('login.showError', error.error)
				}
			});
	}

	render() {
		return (
			<View style={css.wrap}>
				<View style={css.body}>
					<View style={css.wrapForm}>
						<View style={css.textInputWrap}>
							<Text style={css.textLabel}>{Languages.name}</Text>
							<TextInput
								placeholder={Languages.enterName}
								underlineColorAndroid="transparent"
								style={css.textInput}
								onChangeText={(text) => this.setState({name: text})}/>
						</View>


						<View style={css.textInputWrap}>
							<Text style={css.textLabel}>{Languages.email}</Text>
							<TextInput underlineColorAndroid="transparent"
							           placeholder={Languages.enterEmail}
							           style={css.textInput}
							           onChangeText={(text) => this.setState({email: text})}/>
						</View>
						<View style={css.textInputWrap}>
							<Text style={css.textLabel}>{Languages.passwordUp}</Text>
							<TextInput underlineColorAndroid="transparent"
							           placeholder={Languages.enterPassword}
							           style={css.textInput}
							           secureTextEntry={true}
							           onChangeText={(text) => this.setState({password: text})}/>
						</View>

						<View style={css.wrapButton}>
							{this.state.loading?
								<TouchableOpacity style={css.btnLogIn}>
										<Spinner
												size={20}
												type="FadingCircle"
												color="#FFFFFF"/>
								</TouchableOpacity>
							: <TouchableOpacity style={css.btnLogIn}
							                  onPress={this.btnSignUp.bind(this)}>
								<Text style={css.btnLogInText}> {Languages.signup} </Text>
							</TouchableOpacity>}
						</View>
					</View>
				</View>
			</View>
		);
	}
}
