'use strict';
import React, {Component} from "react";
import {TabBarIOS} from "react-native";
import Posts from './Views/Post/Index';
import Photos from './Views/Photo/Index';
import Category from './Views/Category/Index';
import Videos from './Views/Video/Index';
import ReadLater from "@readlater/Index";

import Login from "@login/Index";
import Color from '@common/Color';
import Constants from '@common/Constants';
import AppEventEmitter from "@services/AppEventEmitter";
import User from "@services/User";

export default class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedTab: 'tabNews',
			isLogined: false,
			categoryActive: null
		}
	}

	refreshPage() {
		const self = this;
		User.isLogedIn().then(function (isLogined) {
			self.setState({isLogined: isLogined});
			if (isLogined)  {
				self.setState({selectedTab: 'tabReadLater'});
				AppEventEmitter.emit('readLater.update');
			}
			else {
				self.setState({selectedTab: 'tabLogin'});
			}
		})
	}

	loadHomePage() {
		this.setState({selectedTab: 'tabNews'});
	}

	componentDidMount() {
		const self = this;
		User.isLogedIn().then(function (data) {
			self.setState({isLogined: data});
		});
		AppEventEmitter.addListener('homepage.refresh', this.refreshPage.bind(this));
		AppEventEmitter.addListener('homepage.load', this.loadHomePage.bind(this));
	}

	render() {
		return (
			<TabBarIOS
				itemPositioning='fill'
				tintColor={Color.toolbarTint}
				barTintColor={Color.toolbar}
				translucent={true}>
				<TabBarIOS.Item
					title=""
					icon={{uri: Constants.icons.news, scale: 4}}
					selected={this.state.selectedTab === 'tabNews'}
					onPress={() => {
						this.setState({
							selectedTab: 'tabNews'
						});
					}}>
					<Posts categoryActive={this.state.categoryActive}/>
				</TabBarIOS.Item>

				<TabBarIOS.Item
					title=""
					icon={{uri: Constants.icons.category, scale: 4}}
					selected={this.state.selectedTab === 'tabCategory'}
					onPress={() => {
						this.setState({
							selectedTab: 'tabCategory'
						});
					}}>
					<Category />
				</TabBarIOS.Item>

				<TabBarIOS.Item
					title=""
					icon={{uri: Constants.icons.video, scale: 4}}
					selected={this.state.selectedTab === 'tabVideo'}
					onPress={() => {
						this.setState({
							selectedTab: 'tabVideo'
						});
					}}>
					<Videos />
				</TabBarIOS.Item>

				<TabBarIOS.Item
					title=""
					icon={{uri: Constants.icons.photo, scale: 4}}
					selected={this.state.selectedTab === 'tabPhoto'}
					onPress={() => {
						this.setState({
							selectedTab: 'tabPhoto'
						});
					}}>
					<Photos />
				</TabBarIOS.Item>

				{this.state.isLogined ?
					<TabBarIOS.Item
						title=""
						icon={{uri: Constants.icons.love, scale: 4}}
						selected={this.state.selectedTab === 'tabReadLater'}
						onPress={() => {
							this.setState({
								selectedTab: 'tabReadLater'
							});
							AppEventEmitter.emit('readLater.update');
						}}>
						<ReadLater />
					</TabBarIOS.Item>
					:
					<TabBarIOS.Item
						title=""
						icon={{uri: Constants.icons.love, scale: 4}}
						selected={this.state.selectedTab === 'tabLogin'}
						onPress={() => {
							this.setState({
								selectedTab: 'tabLogin'
							});
						}}>
						<Login />
					</TabBarIOS.Item>
				}
			</TabBarIOS>
		);
	}
}
