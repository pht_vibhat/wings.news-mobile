'use strict';

import React, {Component} from "react";
import {Navigator, Text, View} from "react-native";
import {Scene, Schema, Actions, Animations} from "react-native-router-flux";

import PostDetails from "./Views/PostDetail/Index";
import News from "./Views/Post/Index";
import AppEventEmitter from "@services/AppEventEmitter";

import Home from "./Home";
import Videos from "@videos/Index";
import VideoDetail from "@videodetail/Index";
import Photos from "@photo/Index";
import Category from "@category/Index";
import LogIn from "@login/Index";
import ReadLater from "@readlater/Index";
import SignUp from "@signup/Index";
import CustomPage from "@custompage/Index";
import Edition from '@edition/index'
// import CustomPage from "@custompage/Index";
import Setting from "@setting/Index";
import MenuSide from "./Views/Navigation/MenuScale";
import Languages from "@common/Languages";
import Constants from "@common/Constants";
// import MenuSide from "./Views/Navigation/MenuOverlay";
// import MenuSide from "./Views/Navigation/MenuSmall";
// import MenuSide from "./Views/Navigation/MenuWide";
import SplashScreen from 'react-native-smart-splash-screen'
export default class RootRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {menuStyle: 0};

    //Set Default Language for App
    Languages.setLanguage(Constants.Language);
  }

  changeMenuStyle(data) {
    this.setState({menuStyle: data.menuId})
  }

  componentDidMount() {
    AppEventEmitter.addListener('app.changeMenuStyle', this.changeMenuStyle.bind(this));

    SplashScreen.close({
       animationType: SplashScreen.animationType.scale,
       duration: 850,
       delay: 500,
    });
  }

  render() {
    const scenes = Actions.create(
      <Scene key="scene">
        <Scene key="home" component={Home} title={Languages.home} initial={true} />
        <Scene key="category" component={Category} title={Languages.category} />

        <Scene key="news" component={News} title={Languages.post} />
        <Scene key="postDetails" component={PostDetails} title='Post Detail' />

        <Scene key="videoDetail" component={VideoDetail} title="Video Detail" />
        <Scene key="videos" component={Videos} title="Videos" />
        <Scene key="photos" component={Photos} title="Photos" />

        <Scene key="login" component={LogIn} title={Languages.login} />
        <Scene key="signup" component={SignUp} title={Languages.signup} />
        <Scene key="readlater" component={ReadLater} title={Languages.readlater} />
        <Scene key="custompage" component={CustomPage} title={Languages.about} />
        <Scene key="custompage" component={CustomPage} title={Languages.contact} />
        <Scene key="setting" component={Setting} title={Languages.setting}  />
	      <Scene key="edition" component={Edition} title="Edition"  />
      </Scene>
    );

    return (<MenuSide ref="menuDefault" scenes={scenes} />);
  }

}
