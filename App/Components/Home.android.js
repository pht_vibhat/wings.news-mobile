'use strict';

import React, {Component} from "react";
import ScrollableTabView, {ScrollableTabBar} from "react-native-scrollable-tab-view";
import FacebookTabBar from "@custom/FacebookTabBar";
import AppEventEmitter from "@services/AppEventEmitter";
import Posts from './Views/Post/Index';
import Photos from './Views/Photo/Index';
import Category from './Views/Category/Index';
import Videos from './Views/Video/Index';
import Login from "./Views/LogIn/Index";
import ReadLater from "@readlater/Index";
import User from "@services/User";

export default class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLogined: false,
			categoryActive: null
		}
	}
	refreshPage() {
		const self = this;
		User.isLogedIn().then(function (isLogined) {
			self.setState({isLogined: isLogined});
			if (isLogined)  {
				self.setState({selectedTab: 'tabReadLater'});
				AppEventEmitter.emit('readLater.update');
			}
			else {
				self.setState({selectedTab: 'tabLogin'});
			}
		})
	}

	componentDidMount() {
		const self = this;
		User.isLogedIn().then(function (data) {
			self.setState({isLogined: data});
		});
		AppEventEmitter.addListener('homepage.refresh', this.refreshPage.bind(this));
		AppEventEmitter.addListener('homepage.load', this.loadHomePage.bind(this));
	}

	loadHomePage() {
		this.refs.tabMenu.goToPage(0);
	}

	render() {
		return (
			<ScrollableTabView
				ref="tabMenu"
				tabBarPosition={"bottom"}
				locked={true}
				renderTabBar={() => <FacebookTabBar />}>
				<Posts tabLabel="md-paper"/>
				<Category tabLabel="md-grid"/>
				<Videos tabLabel="md-film"/>
				<Photos tabLabel="md-photos"/>
				{this.refs.tabMenu && this.state.isLogined ?
					<ReadLater tabLabel="md-heart"/>
					:
					<Login tabLabel="md-heart"/>
				}
			</ScrollableTabView>
		);
	}
}
