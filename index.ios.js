/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

'use strict';
import React, {Component} from 'react';
import {AppRegistry, StatusBar} from 'react-native';
import OneSignal from 'react-native-onesignal';

StatusBar.setBarStyle('light-content');
import RootRouter from './App/Components/RootRouter';
import Constants from "@common/Constants";

import {
  setCustomText
} from "react-native-global-props";

const customTextProps = {
  style: {
    fontFamily: Constants.fontFamily,
  }
};
setCustomText(customTextProps);

class beoNews extends Component {
  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('registered', this.onRegistered);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('registered', this.onRegistered);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onRegistered(notifData) {
    console.log("Device had been registered for push notifications!", notifData);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return (
      <RootRouter />
    );
  }
}

AppRegistry.registerComponent('beoNews', () => beoNews);
